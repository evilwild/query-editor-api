export function Author(author_id, author_name) {
		return {
				author_id,
				author_name
		};
}

export default function Element(id, segments, created_at, element_name, author_name) {
		return {
				id,
				segments,
				created_at,
				element_name,
				author_name
		};
}

