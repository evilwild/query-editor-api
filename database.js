const sqlite3 = require('sqlite3').verbose();

class DatabaseDriver {

	db;
	dbName = "biblio.db";

	constructor() {
		this.open();
	}

	create_db() {
		this.db.serialize(() => {
			// create authors table
			db.run(
				`CREATE TABLE IF NOT EXISTS ${getTablename().authors()} 
(author_id INTEGER PRIMARY KEY AUTOINCREMENT, 
author_name VARCHAR(50))`
			);

			// create elements table
			db.run(`
CREATE TABLE IF NOT EXISTS ${getTablename().elements()}
(id INTEGER PRIMARY KEY AUTOINCREMENT, 
segments BLOB NOT NULL,
created_at DATETIME DEFAULT CURRENT_TIMESTAMP, 
element_name TEXT NOT NULL, 
author_id INTEGER,
CONSTRAINT fk_authors 
  FOREIGN KEY (author_id) 
  REFERENCES ${getTablename().authors()}(author_id))
F`);

			// create index on elements(element_name)
			db.run(`CREATE INDEX IF NOT EXISTS idx_element_name on ${getTablename().elements()}(element_name)`);

			// trigger to insert into usage when new elements inserted
			db.run(`CREATE TRIGGER IF NOT EXISTS insert_usage_on_element AFTER INSERT ON ${getTablename().elements()} BEGIN INSERT INTO ${getTablename().usage()} (element_id) VALUES (new.id); END;
`);

			// create usage table
			db.run(`
CREATE TABLE IF NOT EXISTS ${getTablename().usage()}
(element_id INTEGER PRIMARY KEY NOT NULL,
count INTEGER NOT NULL DEFAULT 0,
CONSTRAINT fk_elements
  FOREIGN KEY (element_id)
  REFERENCES ${getTablename().elements()}(id)
  ON DELETE CASCADE)
`);
		});
	}

	open() {
			this.db = new sqlite3.Database(this.dbName);
			this.db.get("PRAGMA foreign_keys = ON");
	}

	close() {
		this.db.close();
	}

	getElementByName(element_name) {
		db.get(`SELECT elements.*, usage.count as usage_count, authors.author_name FROM elements INNER JOIN usage ON usage.element_id=elements.id LEFT JOIN authors ON authors.author_id=elements.author_id WHERE elements.element_name LIKE '%${element_name}%'`, (err, row) => {
			if (err) {
				console.error(err);
				return;
			}
			return row;
		});
	}

	getElementById(id) {
		db.get(`SELECT * FROM elements WHERE id=?`, id, (err, row) => {
			if (err) {
				console.error(err);
				return;
			}
			return row;
		});
	}

	getAllElements() {
		db.all(`SELECT elements.*, usage.count as usage_count, authors.author_name FROM elements INNER JOIN usage on usage.element_id=elements.id LEFT JOIN authors on authors.author_id=elements.author_id ORDER BY usage.count`, (err, rows) => {
			if (err) {
				console.error(err);
				return;
			}
			console.log(rows);
		});
	}

	addElement(element_dto) {
		const preparedStatement = db.prepare(`INSERT INTO elements (segments, element_name, author_id) VALUES(?, ?, ?)`);
		preparedStatement.run(element_dto.segments, element_dto.element_name, element_dto.author_id, (res, error) => {
			if (error) {
				console.error(error);
				return;
			}
		});
		preparedStatement.finalize();
	}

	deleteElementOnId(element_id) {
		var preparedStatement = db.prepare(`DELETE FROM elements WHERE element_id=?`);
		preparedStatement.run(element_id, (res, error) => {
			if (error) {
				console.error(error);
				return;
			}
		});
		preparedStatement.finalize();
	}

	addAuthor(author_name) {
		const preparedStatement = db.prepare(`INSERT INTO authors (author_name) VALUE (?)`);
		preparedStatement.run(author_name, (res, error) => {
			if (error) {
				console.error(error);
				return;
			}
		});
		preparedStatement.finalize();
	}

	getAllAuthors() {
		db.all(`SELECT * FROM authors`, (err, rows) => {
			if (err) {
				console.error(err);
				return;
			}
			console.log(rows);
		});
	}
}

module.exports = DatabaseDriver;
